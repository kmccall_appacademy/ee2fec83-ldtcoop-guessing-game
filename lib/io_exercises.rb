# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.

def guessing_game
  # this is the number the user has to guess
  random_number = rand(100) + 1
  # ths is how many times the user has guessed
  guesses = 0
  # this is the user's current guess in the main function
  current_guess = nil
  # loops until the user guesses right
  until current_guess == random_number
    guesses += 1
    current_guess = guess_get # function
    if current_guess > random_number
      puts "#{current_guess} was too high! Try a smaller number!"
    elsif current_guess < random_number
      puts "#{current_guess} was too low! Try a bigger number!"
    end
  end
  # what happens when the user guesses correctly
  puts "#{current_guess} was correct!"
  puts "It took you #{guesses} to guess that one."
end

def guess_get
  # loops to ask for a valid number
  loop do
    puts "Guess a number!"
    # this is the user's current guess
    guess = gets.chomp
    return guess.to_i if valid_guess?(guess)
    puts "Sorry, that wasn't a valid guess."
    puts "Please enter a number between 1 and 100."
  end
end

def valid_guess?(guess)
  (0..100).map(&:to_s).include?(guess)
end
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def shuffler
  puts "What file would you like to shuffle?"
  doc_name = gets.chomp
  line_array = File.foreach(doc_name).map { |line| line }
  line_array.shuffle!
  File.open("#{doc_name} -- shuffled.txt", "w") do |f|
    f.puts line_array.join
  end
end
